//
//  CalendarResponse.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/27/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import Foundation

struct CalendarResponse: Codable {
	let calendarResponse: [CalendarResponse]?

	struct CalendarResponse: Codable {
		let daysEvents: [DaysEvents]?
	}

	struct DaysEvents: Codable {
		let identifier: Int?
		let start: String?
		let end: String?
		let title: String?
		let styles: String?

		enum CodingKeys: String, CodingKey {
			case identifier = "id"
			case start = "start"
			case end = "end"
			case title = "title"
			case styles = "styles"
		}
	}

}
