//
//  DateExtensions.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/28/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import Foundation

extension Date {
	
	var startOfDay: Date {
		return Calendar.current.startOfDay(for: self)
	}
	
	var endOfDay: Date {
		var components = DateComponents()
		components.day = 1
		components.second = -1
		return Calendar.current.date(byAdding: components, to: startOfDay)!
	}
	
}
