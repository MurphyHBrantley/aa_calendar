//
//  ColorMapper.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/27/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import UIKit

class ColorMapper {
	
	static func mapColor(key: String) -> UIColor {
		switch key {
		case "rap-color event-text-color":
			return UIColor(red: 234/255, green: 176/255, blue: 65/255, alpha: 1.0)
		case "sequence-color event-text-color":
			return UIColor(red: 69/255, green: 191/255, blue: 147/255, alpha: 1.0)
		default:
			return .lightGray
		}
	}
	
}
