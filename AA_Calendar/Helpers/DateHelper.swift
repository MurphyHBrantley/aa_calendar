//
//  DateHelper.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/28/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import UIKit

class DateHelper {
	
	/// Get a date from a date string
	static func dateFromString(_ str: String, dateFormat: String) -> Date? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = dateFormat
		return dateFormatter.date(from: str)
	}
	
	/// Get a formatted string from a date
	static func stringFromDate(_ date: Date, dateFormat: String) -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: dateFormat, options: 0, locale: Calendar.current.locale)
		return dateFormatter.string(from: date)
	}
	
	/// Get hours and minutes as string from a date
	static func hoursMinutesFrom(date: Date) -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm"
		return dateFormatter.string(from: date)
	}
	
	/// Get seconds from date string (HH:mm)
	static func secondsFromString(string: String) -> CGFloat {
		let components = string.components(separatedBy: ":")
		let hours = Int(components[0]) ?? 0
		let minutes = Int(components[1]) ?? 0
		return CGFloat((hours * 3600) + (minutes * 60))
    }

	
	/// Find out if the selected date is between start date and end date
	static func dayEventContainsDate(dayEvent: CalendarResponse.DaysEvents, selectedDate: Date) -> Bool {
		if let start = dayEvent.start, let end = dayEvent.end {
			if let startDate = DateHelper.dateFromString(start, dateFormat: "yyyy-MM-dd HH:mm:ss"), let endDate = DateHelper.dateFromString(end, dateFormat: "yyyy-MM-dd HH:mm:ss") {
				let startOfDay = selectedDate.startOfDay
				let endOfDay = selectedDate.endOfDay
				return (startDate...endDate).contains(startOfDay) || (startDate...endDate).contains(endOfDay)
			}
		}
		return false
	}
	
	/// Check if the beginning and end of the selected day falls within the range of the start date and end date passed into the function
	static func checkIsAllDay(startDateStr: String?, endDateStr: String?, selectedDate: Date) -> Bool {
		
		guard let start = startDateStr, let end = endDateStr else { return false }
		guard let startDate = DateHelper.dateFromString(start, dateFormat: "yyyy-MM-dd HH:mm:ss"), let endDate = DateHelper.dateFromString(end, dateFormat: "yyyy-MM-dd HH:mm:ss") else { return false}
		
		let startOfDay = selectedDate.startOfDay
		let endOfDay = selectedDate.endOfDay
						
		return (startDate...endDate).contains(startOfDay) && (startDate...endDate).contains(endOfDay)
	}
	
}
