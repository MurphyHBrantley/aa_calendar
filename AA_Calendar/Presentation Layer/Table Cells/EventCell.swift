//
//  EventCell.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/27/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import UIKit

struct EventCellDataModel {
	var identifier: Int?
	var eventTitle: String?
	var eventColor: UIColor?
	var startDate: String?
	var endDate: String?
	var isAllDay = false
}

class EventCell: UITableViewCell {

	// MARK: - Outlets
	@IBOutlet weak var startLabel: UILabel!
	@IBOutlet weak var endLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var allDayLabel: UILabel!
	@IBOutlet weak var containerView: UIView!

	@IBOutlet weak var leadingSpace: NSLayoutConstraint!
	@IBOutlet weak var trailingSpace: NSLayoutConstraint!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setLeadingSpace(_ startDate: String?) {
		if let start = startDate {
			leadingSpace.constant = getLeadingSpace(start)
		} else {
			leadingSpace.constant = 0
		}
	}
	
	func setTrailingSpace(_ endDate: String?) {
		if let end = endDate {
			trailingSpace.constant = getTrailingSpace(end)
		} else {
			trailingSpace.constant = 0
		}
	}
	
	/// Calculate the leading space of the container view based on the start time
	private func getLeadingSpace(_ startTime: String) -> CGFloat {
		let seconds = DateHelper.secondsFromString(string: startTime)
		// 86400 seconds in a day
		return CGFloat(seconds / 86400) * self.frame.width
	}
	
	/// Calculate the trailing space of the container view based on the end time
	private func getTrailingSpace(_ startTime: String) -> CGFloat {
		let seconds = DateHelper.secondsFromString(string: startTime)
		// 86400 seconds in a day
		let space = CGFloat(seconds / 86400) * self.frame.width
		return self.frame.width - space
	}
    
}
