//
//  Presenter.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/27/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import Foundation
import UIKit

protocol PresenterProtocol: AnyObject {
	func retrieveCalendarData()
	func dateSelected(date: Date)
}

class Presenter: PresenterProtocol {
	
	weak var viewController: ViewControllerProtocol?
	var calendarData: CalendarResponse?
	
	init(_ vc: ViewControllerProtocol) {
		viewController = vc
	}
	
	/// Set titles for navigation bar and selected date label on the view
	private func setTitles(date: Date) {
        // Set selected month/year (navigation bar title)
		viewController?.populateTitle(DateHelper.stringFromDate(date, dateFormat: "MMMM yyyy"))
		
        // Set selected day (selected date label)
		viewController?.populateSelectedDateTitle(DateHelper.stringFromDate(date, dateFormat: "E, d MMM yyyy"))
	}
	
	/// Retrieve the data for the selected date
	private func retrieveSelectedDateData(_ selectedDate: Date) {
		
		var tableData: [EventCellDataModel] = []

		guard let dayEvents = calendarData?.calendarResponse?.first?.daysEvents else {
			// populate table data in view controller
			viewController?.populateTableData(tableData)
			return
		}
		
		for day in dayEvents {
			if DateHelper.dayEventContainsDate(dayEvent: day, selectedDate: selectedDate) {
				tableData.append(createDayEventDataModel(dayEvent: day, selectedDate: selectedDate))
			}
		}
		// populate table data in view controller
		viewController?.populateTableData(tableData)
	}
	
	/// Creates an EventCell data model from a DaysEvents object
	private func createDayEventDataModel(dayEvent: CalendarResponse.DaysEvents, selectedDate: Date) -> EventCellDataModel {

		// All day bool checks if the start date is before selected date and end date is after selected date
		let isAllDay = DateHelper.checkIsAllDay(startDateStr: dayEvent.start, endDateStr: dayEvent.end, selectedDate: selectedDate)
		
		// Start date must be the same day as selected day to display hours:minutes
		var formattedStart: String?
		if let start = dayEvent.start, let startDate = DateHelper.dateFromString(start, dateFormat: "yyyy-MM-dd HH:mm:ss"), Calendar.current.isDate(startDate, inSameDayAs: selectedDate) {
			formattedStart = DateHelper.hoursMinutesFrom(date: startDate)
		}
		
		// End date must be the same day as selected day to display hours:minutes
		var formattedEnd: String?
		if let end = dayEvent.end, let endDate = DateHelper.dateFromString(end, dateFormat: "yyyy-MM-dd HH:mm:ss"), Calendar.current.isDate(endDate, inSameDayAs: selectedDate) {
			formattedEnd = DateHelper.hoursMinutesFrom(date: endDate)
		}
				
		let eventDataModel = EventCellDataModel(identifier: dayEvent.identifier, eventTitle: dayEvent.title, eventColor: ColorMapper.mapColor(key: dayEvent.styles ?? ""), startDate: formattedStart, endDate: formattedEnd, isAllDay: isAllDay)
		return eventDataModel
	}
	
}

// MARK: - PresenterProtocol functions
extension Presenter {
	
	func dateSelected(date: Date) {
		// Set titles based on selected date
		setTitles(date: date)
		retrieveSelectedDateData(date)
	}
	
	func retrieveCalendarData() {
		CalendarServiceController().retrieveCalendarDate { (response) in
			guard let calendarResponse = response else {
				// TODO: handle error here
				return
			}
			self.calendarData = calendarResponse
		}
	}
	
}
