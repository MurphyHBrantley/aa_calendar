//
//  ViewController.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/27/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import UIKit
import GCCalendar

protocol ViewControllerProtocol: AnyObject {
	func populateTitle(_ title: String)
	func populateSelectedDateTitle(_ title: String)
	func populateTableData(_ data: [EventCellDataModel])
}

class ViewController: UIViewController, ViewControllerProtocol {
    
	// MARK:- Outlets
    @IBOutlet weak var calendarView: GCCalendarView!
    @IBOutlet weak var calendarHeight: NSLayoutConstraint!
    @IBOutlet weak var selectedDateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    // MARK:- Properties
	var presenter: PresenterProtocol?
	var displayMode = UIBarButtonItem()
	var tableData: [EventCellDataModel] = []

	private enum CalendarHeight {
		static let month = 325
		static let week = 64
	}
}

// MARK: - View
extension ViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		presenter = Presenter(self)
		presenter?.retrieveCalendarData()
        
		setupUI()
    }
	
	private func setupUI() {
        let tintColor = UIColor(red: 1.0, green: 0.23, blue: 0.19, alpha: 1.0)
		navigationController!.toolbar.tintColor = tintColor
		navigationController!.navigationBar.tintColor = tintColor
        
		addBottomToolbar()
		setupCalendarView()
		setupTableView()
	}
}

// MARK: - Toolbar
extension ViewController {
    
    fileprivate func addBottomToolbar() {
        navigationController!.isToolbarHidden = false
        
		let today = UIBarButtonItem(title: "Today", style: .plain, target: self, action: #selector(selectToday))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		displayMode = UIBarButtonItem(title: "Week View", style: .plain, target: self, action: #selector(setDisplayMode))
        
        toolbarItems = [today, space, displayMode]
    }
    
    // Toolbar Actions
    @objc func selectToday() {
        calendarView.select(date: Date())
    }
    
    @objc func setDisplayMode() {
        calendarView.displayMode = (calendarView.displayMode == .month) ? .week : .month
		if calendarView.displayMode == .month {
			displayMode.title = "Week View"
			calendarHeight.constant = CGFloat(CalendarHeight.month)
		} else {
			displayMode.title = "Month View"
			calendarHeight.constant = CGFloat(CalendarHeight.week)
		}
		UIView.animate(withDuration: 0.2) {
			self.view.layoutIfNeeded()
		}
    }
}

// MARK: - ViewControllerProtocol Functions
extension ViewController {
	func populateTitle(_ title: String) {
		navigationItem.title = title
	}
	
	func populateSelectedDateTitle(_ title: String) {
		selectedDateLabel.text = title
	}
	
	func populateTableData(_ data: [EventCellDataModel]) {
		tableData = data
		tableView.reloadData()
	}
}

// MARK: - Calendar View
fileprivate extension ViewController {
    func setupCalendarView() {
        calendarView.delegate = self
        calendarView.displayMode = .month
    }
}

// MARK: - GCCalendarViewDelegate
extension ViewController: GCCalendarViewDelegate {
    
    func calendarView(_ calendarView: GCCalendarView, didSelectDate date: Date, inCalendar calendar: Calendar) {
        
		presenter?.dateSelected(date: date)
    }
}

// MARK: - Table View Delegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
	
	private func setupTableView() {
		tableView.delegate = self
		tableView.dataSource = self
		
		tableView.estimatedRowHeight = 80
		tableView.rowHeight = UITableView.automaticDimension
		
		tableView.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		tableData.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as? EventCell else { return UITableViewCell() }
		
		cell.selectionStyle = .none
		
		let dataModel = tableData[indexPath.row]
		
		cell.startLabel.text = dataModel.startDate
		cell.endLabel.text = dataModel.endDate
		cell.titleLabel.text = dataModel.eventTitle
		cell.containerView.backgroundColor = dataModel.eventColor
		cell.allDayLabel.isHidden = !dataModel.isAllDay
		
		cell.setLeadingSpace(dataModel.startDate)
		cell.setTrailingSpace(dataModel.endDate)
		
		return cell
	}
	
}
