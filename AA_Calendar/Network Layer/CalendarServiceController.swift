//
//  CalendarServiceController.swift
//  AA_Calendar
//
//  Created by Murphy Brantley on 1/27/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import Foundation

class CalendarServiceController {
	
	func retrieveCalendarDate(completion:@escaping (_ response: CalendarResponse?) -> Void) {
		completion(loadJson(filename: "CalendarData"))
	}
	
	private func loadJson(filename fileName: String) -> CalendarResponse? {
		if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
			do {
				let data = try Data(contentsOf: url)
				let decoder = JSONDecoder()
				let jsonData = try decoder.decode(CalendarResponse.self, from: data)
				return jsonData
			} catch {
				print("error:\(error)")
			}
		}
		return nil
	}
}
