//
//  ColorMapperTests.swift
//  AA_CalendarTests
//
//  Created by Murphy Brantley on 1/28/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import XCTest
@testable import AA_Calendar

class ColorMapperTests: XCTestCase {
	
    override func setUp() {}

	override func tearDown() {}

    func testColorMapper() {
		let color1 = ColorMapper.mapColor(key: "rap-color event-text-color")
		XCTAssertEqual(color1, UIColor(red: 234/255, green: 176/255, blue: 65/255, alpha: 1.0), "colors are not equal")
		
		let color2 = ColorMapper.mapColor(key: "sequence-color event-text-color")
		XCTAssertEqual(color2, UIColor(red: 69/255, green: 191/255, blue: 147/255, alpha: 1.0), "colors are not equal")

		let color3 = ColorMapper.mapColor(key: "random key")
		XCTAssertEqual(color3, UIColor.lightGray, "colors are not equal")
    }
	
}
