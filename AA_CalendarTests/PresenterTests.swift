//
//  PresenterTests.swift
//  AA_CalendarTests
//
//  Created by Murphy Brantley on 1/28/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import XCTest
@testable import AA_Calendar

class PresenterTests: XCTestCase {

	// MARK: - Properties
	private var mockView: ViewControllerMock!
	private var sut: Presenter!
	
    override func setUp() {
		super.setUp()
		mockView = ViewControllerMock()
		sut = Presenter(mockView)
		sut.retrieveCalendarData()
	}

    override func tearDown() {
		super.tearDown()
		mockView = nil
		sut = nil
	}

    func testDateSelected() {
		// ARRANGE
		var dateComponents = DateComponents()
		dateComponents.year = 2020
		dateComponents.month = 1
		dateComponents.day = 27
		dateComponents.hour = 8
		dateComponents.minute = 34
		// Create date from components
		let userCalendar = Calendar.current // user calendar
		let date = userCalendar.date(from: dateComponents)!
		
		let titleTextExpectation = XCTestExpectation(description: "title text expectation")
		let selectedDateTitleExpectation = XCTestExpectation(description: "selected date title text expectation")
		let tableDataExpectation = XCTestExpectation(description: "table data expectation")
		
		mockView.titleTextExpectation = titleTextExpectation
		mockView.selectedDateTitleExpectation = selectedDateTitleExpectation
		mockView.tableDataExpectation = tableDataExpectation
		
		// ACT
		sut.dateSelected(date: date)
		
		// ASSERT
		wait(for: [titleTextExpectation, selectedDateTitleExpectation, tableDataExpectation], timeout: 1)
		XCTAssertEqual(mockView.titleText!, "January 2020", "title text is incorrect")
		XCTAssertEqual(mockView.selectedDateTitleText!, "Mon, Jan 27, 2020", "selected date title text is incorrect")
		XCTAssertEqual(mockView.tableData.count, 1, "table data count is incorrect")
		XCTAssertFalse(mockView.tableData.first!.isAllDay, "is all day is broken for this event")
		XCTAssertEqual(mockView.tableData.first!.eventTitle!, "RAP B", "event title is incorrect")

	}

}
