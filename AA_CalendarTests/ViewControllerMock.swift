//
//  ViewControllerMock.swift
//  AA_CalendarTests
//
//  Created by Murphy Brantley on 1/28/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import XCTest
@testable import AA_Calendar

class ViewControllerMock: UIViewController, ViewControllerProtocol {
	
	// MARK: - Properties
	var presenter: PresenterProtocol?
	var tableData: [EventCellDataModel] = []
	var titleText: String?
	var selectedDateTitleText: String?
	
	var titleTextExpectation: XCTestExpectation?
	var selectedDateTitleExpectation: XCTestExpectation?
	var tableDataExpectation: XCTestExpectation?

	// MARK:- Functions
	override func viewDidLoad() {
		presenter = Presenter(self)
	}
	
	func populateTitle(_ title: String) {
		titleText = title
		titleTextExpectation?.fulfill()
	}
	
	func populateSelectedDateTitle(_ title: String) {
		selectedDateTitleText = title
		selectedDateTitleExpectation?.fulfill()
	}
	
	func populateTableData(_ data: [EventCellDataModel]) {
		tableData = data
		tableDataExpectation?.fulfill()
	}
	
}
