//
//  DateHelperTests.swift
//  AA_CalendarTests
//
//  Created by Murphy Brantley on 1/28/20.
//  Copyright © 2020 Murphy Brantley. All rights reserved.
//

import XCTest
@testable import AA_Calendar

class DateHelperTests: XCTestCase {

	var myTestableDate: Date {
		// Create a date object to use for the tests
		var dateComponents = DateComponents()
		dateComponents.year = 1980
		dateComponents.month = 7
		dateComponents.day = 11
		dateComponents.hour = 8
		dateComponents.minute = 34

		// Create date from components
		let userCalendar = Calendar.current // user calendar
		return userCalendar.date(from: dateComponents)!
	}
		
    override func setUp() {}
	
    override func tearDown() {}
	

    func testStringFromDate() {
		let format1 = DateHelper.stringFromDate(myTestableDate, dateFormat: "HH:mm")
		XCTAssertEqual(format1, "08:34", "date string is incorrect")

		let format2 = DateHelper.stringFromDate(myTestableDate, dateFormat: "E, d MMM yyyy")
		XCTAssertEqual(format2, "Fri, Jul 11, 1980", "date string is incorrect")
		
		let format3 = DateHelper.stringFromDate(myTestableDate, dateFormat: "MMMM yyyy")
		XCTAssertEqual(format3, "July 1980", "date string is incorrect")
	}
	
	func testSecondFromString() {
		let seconds1 = DateHelper.secondsFromString(string: "08:34")
		XCTAssertEqual(seconds1, CGFloat(30840.0), "seconds are not equal")
		
		let seconds2 = DateHelper.secondsFromString(string: "00:01")
		XCTAssertEqual(seconds2, CGFloat(60.0), "seconds are not equal")
	}
	
	func testDayEventContainsDateTrue() {
		var dateComponents = DateComponents()
		dateComponents.year = 2020
		dateComponents.month = 1
		dateComponents.day = 27
		dateComponents.hour = 8
		dateComponents.minute = 34

		// Create date from components
		let userCalendar = Calendar.current // user calendar
		let date = userCalendar.date(from: dateComponents)!
		
		let dayEvent = CalendarResponse.DaysEvents(identifier: nil, start: "2020-01-27 06:00:00", end: "2020-01-28 18:00:00", title: nil, styles: nil)
		
		let contains = DateHelper.dayEventContainsDate(dayEvent: dayEvent, selectedDate: date)
		XCTAssertTrue(contains, "day event does not contain selected date")

	}
	
	func testDayEventContainsDateFalse() {
		var dateComponents = DateComponents()
		dateComponents.year = 2020
		dateComponents.month = 1
		dateComponents.day = 26
		dateComponents.hour = 8
		dateComponents.minute = 34

		// Create date from components
		let userCalendar = Calendar.current // user calendar
		let date = userCalendar.date(from: dateComponents)!
		
		let dayEvent = CalendarResponse.DaysEvents(identifier: nil, start: "2020-01-27 06:00:00", end: "2020-01-28 18:00:00", title: nil, styles: nil)
		
		let contains = DateHelper.dayEventContainsDate(dayEvent: dayEvent, selectedDate: date)
		XCTAssertFalse(contains, "day event contains selected date")
	}

}
